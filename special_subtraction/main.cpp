/*Exercise: Subtract without being able to subtract*/
#include <stdio.h>
#include <stdlib.h>


bool binary_operation(bool first, bool second, bool &result)
{
	bool take = false;
	if(first == 0 && second == 0)
	{
		result = 0;
	} 
	else if(first == 1 && second == 0)
	{
		result = 1;
	}
	else if(first == 1 && second == 1)
	{
		result = 0;
	}
	else if(first == 0 && second == 1)
	{
		result = 1;
		take = true;
	}
	return take;
}

bool binary_rest(bool first_num, bool second_num)
{
	bool take = false;
	for(int cnt = 0; cnt < 5; cnt++)
	{
		if(take)
		{
			bool result_take = binary_operation(first_num[cnt], true, take);
			result = binary_operation(result_take, second_num[cnt], take);
		}
		else
		{
			bool result = binary_operation(first_num[cnt], second_num[cnt], take);
		}
	}
}

int main() 
{
	bool first_num[5] = {true, false, false, false, true};
	bool second_num[5] = {false, true, false, true, false};

	bool result;

	binary_operation(true, false, result);

	printf("%d", result);


	return EXIT_SUCCESS;
}